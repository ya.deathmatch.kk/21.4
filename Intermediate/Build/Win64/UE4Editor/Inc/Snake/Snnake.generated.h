// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_Snnake_generated_h
#error "Snnake.generated.h already included, missing '#pragma once' in Snnake.h"
#endif
#define SNAKE_Snnake_generated_h

#define Snake_Source_Snake_Snnake_h_26_SPARSE_DATA
#define Snake_Source_Snake_Snnake_h_26_RPC_WRAPPERS
#define Snake_Source_Snake_Snnake_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_Snnake_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnnake(); \
	friend struct Z_Construct_UClass_ASnnake_Statics; \
public: \
	DECLARE_CLASS(ASnnake, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ASnnake)


#define Snake_Source_Snake_Snnake_h_26_INCLASS \
private: \
	static void StaticRegisterNativesASnnake(); \
	friend struct Z_Construct_UClass_ASnnake_Statics; \
public: \
	DECLARE_CLASS(ASnnake, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ASnnake)


#define Snake_Source_Snake_Snnake_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnnake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnnake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnnake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnnake(ASnnake&&); \
	NO_API ASnnake(const ASnnake&); \
public:


#define Snake_Source_Snake_Snnake_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnnake(ASnnake&&); \
	NO_API ASnnake(const ASnnake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnnake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnnake)


#define Snake_Source_Snake_Snnake_h_26_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_Snnake_h_23_PROLOG
#define Snake_Source_Snake_Snnake_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Snnake_h_26_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_Snnake_h_26_SPARSE_DATA \
	Snake_Source_Snake_Snnake_h_26_RPC_WRAPPERS \
	Snake_Source_Snake_Snnake_h_26_INCLASS \
	Snake_Source_Snake_Snnake_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_Snnake_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Snnake_h_26_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_Snnake_h_26_SPARSE_DATA \
	Snake_Source_Snake_Snnake_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_Snnake_h_26_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_Snnake_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class ASnnake>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_Snnake_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> SNAKE_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
